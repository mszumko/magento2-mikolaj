<?php

namespace Kaliop\Storelocator\Block\Index;

use Magento\Framework\View\Element\Template;

class Storelocator extends \Magento\Framework\View\Element\Template
{
    /**
     * Helloworld constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context,
                                array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getHelloWorldTxt()
    {
        return 'Hello World!!!';
    }
}

