<?php

namespace Kaliop\Helloworld\Block\Storeview;

use Kaliop\Helloworld\Model\PostFactory;
use Magento\Framework\View\Element\Template;

class Fullview extends \Magento\Framework\View\Element\Template
{
    protected $_postFactory;

    /**
     * Helloworld constructor.
     * @param Template\Context $context
     * @param PostFactory $postFactory
     * @param array $data
     */
    public function __construct(Template\Context $context,
                                PostFactory $postFactory,
                                array $data = [])
    {
        $this->_postFactory = $postFactory;
        parent::__construct($context, $data);
    }


    /**
     * @param $id
     * @return $Kaliop\Helloworld\Model\
     */
    public function preModel($id)
    {
        $postModel = $this->_postFactory->create();
        $model = $postModel->load($id);
        return $model;
    }
}