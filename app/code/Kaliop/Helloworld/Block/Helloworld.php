<?php

namespace Kaliop\Helloworld\Block;

use Kaliop\Helloworld\Model\PostFactory;
use Magento\Framework\View\Element\Template;

class Helloworld extends \Magento\Framework\View\Element\Template
{
    protected $_postFactory;

    /**
     * Helloworld constructor.
     * @param Template\Context $context
     * @param PostFactory $postFactory
     * @param array $data
     */
    public function __construct(Template\Context $context,
                                PostFactory $postFactory,
                                array $data = [])
    {
        $this->_postFactory = $postFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getHelloWorldTxt()
    {
        return 'Hello World!!!';
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function preCollection()
    {
        $post = $this->_postFactory->create();
        $collection = $post->getCollection();
        return $collection;
    }
}
