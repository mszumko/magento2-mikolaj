<?php

namespace Kaliop\Helloworld\Model;

class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'kaliop_helloworld_post';

    protected $_cacheTag = 'kaliop_helloworld_post';

    protected $_eventPrefix = 'kaliop_helloworld_post';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Kaliop\Helloworld\Model\ResourceModel\Post');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}