<?php

namespace Kaliop\Storelocator\Controller\Adminhtml\Storelocator;

use \Magento\Framework\App\Action\Action;
use Magento\Setup\Exception;

class Delete extends Action
{

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if(!empty($params)){
            $store = $this->_objectManager->create('Kaliop\Storelocator\Model\Storelocator')->load($params['id']);
            if(!empty($store)){
                try {
                    $store->delete();
                    $this->messageManager->addSuccess(__('Your store has been deleted !'));
                } catch (Exception $e){
                    $this->messageManager->addError(__('Error while trying to delete store: '));
                }
            }
        }
        return $this->_redirect('*/*/index');
    }
}