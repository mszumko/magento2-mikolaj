<?php

namespace Kaliop\Storelocator\Model\ResourceModel;


class Storelocator extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Post constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('storelocator', 'id');
    }

}