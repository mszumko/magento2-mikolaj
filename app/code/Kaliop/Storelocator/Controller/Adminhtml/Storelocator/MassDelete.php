<?php
namespace Kaliop\Storelocator\Controller\Adminhtml\Storelocator;

use Magento\Backend\App\Action;

class MassDelete extends Action
{

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $count = 0;

        $params = $this->getRequest()->getParams();
        foreach ($params['selected'] as $id) {
            if (!($store = $this->_objectManager->create('Kaliop\Storelocator\Model\Storelocator')->load($id))) {
                $this->messageManager->addError(__('Unable to proceed. Please, try again.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index', array('_current' => true));
            }
            try {
                $store->delete();
                $count++;
            } catch (Exception $e) {
                $this->messageManager->addError(__('Error while trying to delete store: '));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/index', array('_current' => true));
            }
        }
        $this->messageManager->addSuccess(__("A total of {$count} element(s) have been deleted."));
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}