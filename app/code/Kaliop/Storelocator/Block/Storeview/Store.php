<?php

namespace Kaliop\Storelocator\Block\Storeview;

use Kaliop\Storelocator\Model\StorelocatorFactory;
use Kaliop\Storelocator\Helper\Data;
use Magento\Framework\View\Element\Template;

class Store extends \Magento\Framework\View\Element\Template
{
    protected $_storelocatorFactory;

    protected $_helper;
    /**
     * Store constructor.
     * @param Template\Context $context
     * @param StorelocatorFactory $storelocatorFactory
     * @param array $data
     */
    public function __construct(Template\Context $context,
                                Data $helper,
                                StorelocatorFactory $storelocatorFactory,
                                array $data = [])
    {
        $this->_helper = $helper;
        $this->_storelocatorFactory = $storelocatorFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function preCollection()
    {
        $post = $this->_storelocatorFactory->create();
        $collection = $post->getCollection();
        return $collection;
    }

    public function isGoogleMapIsEnable()
    {
        return $this->_helper->getGoogleMapIsEnableConfigValue() == 1 ? true : false;
    }
}