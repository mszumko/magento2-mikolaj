<?php

namespace Kaliop\Storelocator\Controller\Adminhtml\Storelocator;

class Add extends \Magento\Framework\App\Action\Action
{
    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $storeData = $this->getRequest()->getParam('storelocator');
        if (is_array($storeData)) {
            $model = $this->_objectManager->create('Kaliop\Storelocator\Model\Storelocator');
            $model->setData($storeData)->save();
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        };
    }
}