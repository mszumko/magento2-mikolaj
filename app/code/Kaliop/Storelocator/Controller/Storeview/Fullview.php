<?php

namespace Kaliop\Storelocator\Controller\Storeview;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NotFoundException;
use Kaliop\Storelocator\Helper\Data;


class Fullview extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    protected $_helper;
    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(Context $context,
                                Data $helper,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_helper = $helper;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }


    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     * @throws NotFoundException
     */
    public function execute()
    {
        if($this->_helper->redirectIfDisable())
        {
            throw new NotFoundException(__('Parameter is incorrect.'));
        }

        return $this->_resultPageFactory->create();
    }
}
