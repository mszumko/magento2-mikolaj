<?php

namespace Kaliop\Storelocator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * @return mixed
     */
    public function getEnableConfigValue()
    {
        // get enable parameter from configuration, default = 1
        return $this->scopeConfig->getValue('storelocatorOptions/general/enable');
    }

    public function getGoogleMapIsEnableConfigValue()
    {
        return $this->scopeConfig->getValue('storelocatorOptions/general/googleMapIsEnable');
    }

    /**
     * @return bool
     */
    public function redirectIfDisable()
    {
        //return true if module is disable
        return $this->getEnableConfigValue() != 1 ? true : false;
    }
}
