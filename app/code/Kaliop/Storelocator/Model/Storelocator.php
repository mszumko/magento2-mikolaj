<?php

namespace Kaliop\Storelocator\Model;

class Storelocator extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'kaliop_storelocator_post';

    protected $_cacheTag = 'kaliop_storelocator_post';

    protected $_eventPrefix = 'kaliop_storelocator_post';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Kaliop\Storelocator\Model\ResourceModel\Storelocator');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}