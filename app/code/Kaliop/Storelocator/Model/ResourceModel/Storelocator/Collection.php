<?php

namespace Kaliop\Storelocator\Model\ResourceModel\Storelocator;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'kaliop_storelocator_storelocator_collection';
    protected $_eventObject = 'storelocator_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Kaliop\Storelocator\Model\Storelocator', 'Kaliop\Storelocator\Model\ResourceModel\Storelocator');
    }

}