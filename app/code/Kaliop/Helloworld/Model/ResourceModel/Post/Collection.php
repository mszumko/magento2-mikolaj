<?php

namespace Kaliop\Helloworld\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'shop_id';
    protected $_eventPrefix = 'kaliop_helloworld_post_collection';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Kaliop\Helloworld\Model\Post', 'Kaliop\Helloworld\Model\ResourceModel\Post');
    }

}