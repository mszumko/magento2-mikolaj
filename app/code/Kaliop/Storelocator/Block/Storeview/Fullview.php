<?php

namespace Kaliop\Storelocator\Block\Storeview;

use Kaliop\Storelocator\Model\StorelocatorFactory;
use Magento\Framework\View\Element\Template;

class Fullview extends \Magento\Framework\View\Element\Template
{
    protected $_storelocatorFactory;

    /**
     * Fullview constructor.
     * @param Template\Context $context
     * @param StorelocatorFactory $storelocatorFactory
     * @param array $data
     */
    public function __construct(Template\Context $context,
                                StorelocatorFactory $storelocatorFactory,
                                array $data = [])
    {
        $this->_storelocatorFactory = $storelocatorFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param $id
     * @return $this
     */
    public function preModel($id)
    {
        $postModel = $this->_storelocatorFactory->create();
        $model = $postModel->load($id);
        return $model;
    }
}